#!/bin/sh
#@# Following :
#@#   - https://cartodb.readthedocs.io/en/stable/install.html
#@# OS: Ubuntu 16.04 xenial

# -e : Exit immediately if a command exits with a non-zero status.
set -e

# VARIABLES
export CARTODB_INSTALL_WORKDIR="/opt/cartodb"
export RUBY_PATH="/opt/rubies/ruby-2.2.3/bin"
export CARTODB_ENVIRONMENT="production"

# Include functions.inc
# shellcheck source=functions.inc.sh
. ./functions.inc.sh

## Do not run this script as root
check_user

# RUN init.sh
# shellcheck source=init.sh
sudo ./init.sh

## Set Locale
sudo locale-gen en_US.UTF-8
sudo update-locale LANG=en_US.UTF-8 LC_ALL=en_US.UTF-8 LANGUAGE=en_US:en
source /etc/default/locale #>!!! REMEMBER to source locales when switch USER first time

## apt-get install requirements
sys_upgrade
sys_install make pkg-config git

## PostgreSQL
sudo add-apt-repository -y ppa:cartodb/postgresql-10 && sys_update
sys_install postgresql-10 postgresql-plpython-10 postgresql-server-dev-10
sudo sed -i 's/\(peer\|md5\)/trust/' /etc/postgresql/10/main/pg_hba.conf
service_restart postgresql
sudo createuser publicuser --no-createrole --no-createdb --no-superuser -U postgres
sudo createuser tileuser --no-createrole --no-createdb --no-superuser -U postgres

## Install CartoDB postgresql extension
cd "$CARTODB_INSTALL_WORKDIR"
git clone https://github.com/CartoDB/cartodb-postgresql.git
cd cartodb-postgresql
### git checkout <LATEST cartodb-postgresql tag>
git fetch --tags
# shellcheck disable=SC2006,SC2046
latestTag=$(git describe --tags `git rev-list --tags --max-count=1`)
git checkout "$latestTag"
### install with make
sudo make all install

# GIS dependencies
sudo add-apt-repository -y ppa:cartodb/gis && sys_update
sys_install gdal-bin libgdal-dev

# PostGIS
sys_install postgis
sudo createdb -T template0 -O postgres -U postgres -E UTF8 template_postgis
psql -U postgres template_postgis -c 'CREATE EXTENSION postgis;CREATE EXTENSION postgis_topology;'
sudo ldconfig
